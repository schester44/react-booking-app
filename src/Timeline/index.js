import React, { Component } from "react"
import moment from "moment";
import classNames from "classnames"


import TimeIndicator from "./components/TimeIndicator"
import TimelineItem from "./components/TimelineItem"

import { timeToPosition } from "./helpers"

import "./Timeline.css"

class Timeline extends Component {
	constructor() {
		super()

		this.state = {
			blocks: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
			showAMPM: true,
			indicatorPosition: 51,
			indicatorVisible: true
		}

		this.onFocusEvent = this.onFocusEvent.bind(this)
	}

	onFocusEvent(time) {
		this.focus( timeToPosition(time) - 450 )
	}

	componentDidMount() {
		this.setIndicatorPosition();
		
		if (this.props.emitter) {
			this.emitterToken = this.props.emitter.addListener("focus-timeline", this.onFocusEvent)
		}
	}
	
	componentWillUnmount() {
		this.indicatorInterval && clearInterval(this.indicatorInterval)

		if (this.emitterToken) {
			this.emitterToken.remove();
		}
	}

	focus(position) {
		this.refs.timeline.scrollTop = position;
	}

	setIndicatorPosition() {
		const update = (indicatorPosition) => {
			this.setState({
				indicatorPosition
			})
		}

		let position = timeToPosition(moment().hour() + parseFloat((moment().minute() / 60).toFixed(2)));

		update(position);

		this.indicatorInterval = setInterval(() => {
			const currentTime = moment().hour() + parseFloat((moment().minute() / 60).toFixed(2));

			update(timeToPosition(currentTime))
		}, 1000 * 15);		
		
		// 300 is the estimated height of the Timeline. This value should come from a ref node instead
		this.focus(position - 300)
	}

	render() {
		const { roomStatus } = this.props
		const { indicatorPosition, blocks } = this.state

		let timelineClasses = { "Timeline": true };
		
		if (this.props.appendClassName) {
			timelineClasses[this.props.appendClassName] = true
		}
	
		const TimelineClassNames = classNames(timelineClasses);
		
		const timeBlocks = blocks.map((block, key) => {
			const hour = block > 12 ? block - 12 : block;
			const ampm = block >= 12 ? "pm" : "am";

			return (
				<div key={key} className="time-block">
					<div className="time">
						<div className="primary">
							<div>{ hour }</div>
							{ ampm }
						</div>
						<div className="secondary">
							<div>{hour}:30</div>
						</div>
					</div>
					<div className="segment"></div>
				</div>
			)
		})

		return (
			<div ref="timeline" className={TimelineClassNames}>
				<TimeIndicator status={roomStatus} position={indicatorPosition} />
				<div className="time-segments">
					{
						this.props.events && this.props.events.map((event, key) => (event.duration > 0 && <TimelineItem key={key} item={event} />))
					}

					{timeBlocks}
				</div>
			</div>
		)
	}
}

export default Timeline
