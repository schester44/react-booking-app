// 58 is just some padding to make everything line up correctly
// 135 is the default timeSegment height.. if the CSS every changes then this value will need updated
export const timeToPosition = time => 50 + (time - 7) * 135
