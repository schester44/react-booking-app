import React from "react"

import classNames from "classnames";

// this should go somewhere better... not sold on the app structure
import { formatNumberToTime } from "../../../Room/helpers";

import "./TimelineItem.css"

const TimelineItem = props => {
	const { title, start, end, duration, status } = props.item

	// TODO - Probably shouldnt be hardcoded since these values come from .CSS height and padding values
	const segmentHeight = 135
	const topMargin = 50

	// get its position based on a segments size times its actual start position on the timeline.
	// its start - 7 because the starting hour is 7.. if the starting hour is different than 7 then this needs updated.
	// TODO - this probably should be hardcoded
	const position = topMargin + ((start - 7) * segmentHeight)
	const height = (end - start) * segmentHeight

	const displayClass = {
		TimelineItem: true,
		pending: status === "pending",
		large: duration >= 1,
		medium: duration < 1 && duration > 0.5,
		small: duration <= 0.5
	}

	return (
		<div
			className={classNames(displayClass)}
			style={{ top: `${position}px`, height: `${height}px` }}
		>
			<div className="item-time">
				<p>{formatNumberToTime(start, true)} - {formatNumberToTime(end, true)}</p>
			</div>
			
			<div className="item-title">{title}</div>
		</div>
	)
}

export default TimelineItem
