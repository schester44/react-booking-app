import React from 'react';
import classNames from "classnames";

import "./TimeIndicator.css"

const TimeIndicator = (props) => {
	const classes = {
		TimeIndicator: true,
		busy: props.status !== "available",
		available: props.status === "available"
	}

	return (<div className={ classNames(classes) } style={{ top: `${props.position}px` }}></div>)
}

export default TimeIndicator;
