import React from "react"
import { Link } from "react-router-dom"

import { getRoomStatus } from "../../../Room/helpers"
import Timeline from "../../../Timeline"

import "./Room.css"

const Room = props => {
	const { room, events } = props

	let roomStatus

	if (Array.isArray(events)) {
		roomStatus = getRoomStatus(events)
	}

	return (
		<div className="RoomsList__Room">
			<h2>{room.name}</h2>
			<Timeline
				events={Array.isArray(events) ? events : []}
				appendClassName="Timeline--small"
				roomStatus={roomStatus ? "unavailable" : "available"}
			/>

			<Link className="btn btn--brand btn--lean btn--rounded" role="button" to={`/room/${room.id}`}>
				Book This Room
			</Link>
		</div>
	)
}

export default Room
