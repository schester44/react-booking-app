import React, { Component } from "react"

import Room from "./components/Room"

import "./RoomsList.css"

class RoomsList extends Component {
	
	
	render() {

		const { rooms, events } = this.props;

		const roomsList = rooms.map((room, key) => (<Room room={room} key={key} events={events[room.id]}/>))

		return (
			<div className="RoomsList">
			{ roomsList.length === 0 &&
				<div className="placeholder">No rooms available. Have you configured the app?</div>
			 }

				{ roomsList }
			</div>
		)
	}
}

export default RoomsList
