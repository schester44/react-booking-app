import React, { Component } from "react"
import { Route, Redirect } from "react-router-dom"

import CustomProperties from "react-custom-properties"

import Noty from "noty"
import "noty/lib/noty.css"

import Header from "./components/Header/"
import Room from "../Room/"
import RoomsList from "../RoomsList/"

import { formatThemeProperties } from "./helpers"
import "./App.css"

class App extends Component {
	constructor() {
		super()

		this.api = {
			url: '../index.php/api'
		}

		this.state = {
			disconnected: false,
			defaultRoom: undefined,
			rooms: [],
			device: undefined,
			events: {},
			themes: [],
			theme: undefined
		}

		this.createEvent = this.createEvent.bind(this)
		this.updateEvents = this.updateEvents.bind(this)
		this.pushEvent = this.pushEvent.bind(this)
		this.removePendingEvents = this.removePendingEvents.bind(this)
	}

	async componentWillMount() {
		await this.RoomList()
		await this.EventsList()
		await this.Themes()

		this.setDefaultRoom()
		this.startCheckins()
	}

	async RoomList() {
		const rooms = await fetch(`${this.api.url}/rooms`).then(res => res.json())
		this.setState({ rooms })
		return rooms
	}

	async EventsList() {
		const events = await fetch(`${this.api.url}/events`).then(res => res.json())
		this.setState({ events })
		return events
	}

	async Themes() {
		const themes = await fetch(`${this.api.url}/themes`).then(res => res.json())

		this.setState({ themes })
		return themes
	}

	startCheckins() {
		if (this.eventsInt) {
			clearInterval(this.eventsInt)
		}

		// every 30 seconds, check in and get the latest events
		this.eventsInt = setInterval(async () => {
			console.log(`Fetching Events...`)

			const allEvents = await fetch(`${this.api.url}/events`).then(res => res.json())

			let newEvents = {}

			// loop through the events.
			// if there is a pending event, then add it to the list of newEvents so its not lost
			Object.keys(allEvents).forEach(key => {
				const currentEvents = this.state.events[key]

				if (Array.isArray(currentEvents)) {
					const pendingEvent = currentEvents.find(event => event.status === "pending")
					newEvents[key] = pendingEvent ? [...allEvents[key], pendingEvent] : allEvents[key]
				}

			})

			this.setState({ events: newEvents })
		}, 30000)
	}

	setDefaultRoom() {
		try {
			//eslint-disable-next-line
			if (top.CCHDAPI) {
				//eslint-disable-next-line
				const device_id = top.CCHDAPI.getDeviceID()
				const room = this.state.rooms.find(room => room.device_id.toString() === device_id.toString())

				if (typeof room.device_id !== "undefined") {
					this.setState({
						defaultRoom: room.id
					})
				}
			}
		} catch (e) {
			console.log("CCHDAPI does not exist")
		}

		if (this.state.defaultRoom === undefined) {
			// this could also be done using this.props.match.params,
			// which is probably the correct way however this.props.match does not exist in App
			const parts = window.location.hash.split("/")

			// set the default room, only if we're on a room's page, based on the room id in the url.
			if (typeof parts[2] !== "undefined" && parts[1].toLowerCase() === "room") {
				this.setState(oldState => {
					const defaultRoom = parseInt(parts[2], 10)
					return { defaultRoom }
				})
			}
		}

		const defaultRoom = this.state.rooms.find(room => room.id === this.state.defaultRoom)
		let roomTheme

		if (defaultRoom) {
			roomTheme = this.state.themes.find(theme => theme.id.toString() === defaultRoom.theme_id.toString())
		}

		// get the room's theme or fallback to the default theme
		const theme = roomTheme ? roomTheme : this.state.themes[0]

		this.setState({ theme })
	}

	updateEvents(room, roomEvents) {
		const events = this.state.events

		if (typeof events[room] === "undefined") {
			events[room] = []
		}

		events[room] = roomEvents
		this.setState({ events })
	}

	pushEvent(room, event) {
		const events = this.state.events

		if (!Array.isArray(events[room])) {
			events[room] = []
		}

		events[room].push(event)
		this.setState({ events })
	}

	removePendingEvents(room) {
		// remove any pending events from the currentRoom's events
		const events = { ...this.state.events, [room]: this.state.events[room].filter(event => event.status !== "pending" ) }

		this.setState({ events });
	}

	createEvent(room, callback) {
		// When creating a room, we pack the event into this.state.events[room]
		// Here, we're looking for the pending event, it should be the one we're trying to submit
		let event = this.state.events[room].find(event => event.status === "pending")

		// the backend needs the room_id so we can assign this event to a room.
		event = { ...event, room_id: room }

		// send it!
		fetch(`${this.api.url}/events/${room}`, {
			method: "POST",
			body: JSON.stringify(event)
		})
			.then(res => {
				// consider a 201 a success,
				if (res.status === 201) {
					return true
				} else {
					// failed, remove th event and throw a toast
					this.removePendingEvents(room)

					new Noty({
					    type: 'error',
					    layout: 'topRight',
					    timeout: 3000,
					    progressBar: false,
					    killer: true,
					    text: res.body.error ? res.body.error : "Failed to create the event."
					}).show();
				}
			})
			.then(created => {
				if (created) {
					this.setState(oldState => {
						// find the pending event we just created (there will only ever be 1 pending event)
						// then set its status to "existing"
						let index = oldState.events[room].findIndex(event => event.status === "pending")
						let events = { ...oldState.events }
						events[room][index].status = "existing"

						return { events }
					})

					if (typeof callback === "function") {
						callback()
					}

					new Noty({
					    type: 'success',
					    layout: 'topRight',
					    timeout: 3000,
					    progressBar: false,
					    killer: true,
					    text: "Event Created."
					}).show();
				}
			})
			.catch(err => {
					this.removePendingEvents(room);

					new Noty({
					    type: 'error',
					    layout: 'topRight',
					    timeout: 3000,
					    progressBar: false,
					    killer: true,
					    text: err.body.error ? err.body.error : "Failed to create the event."
					}).show();
			})
	}

	render() {
		const { rooms, theme, events = [] } = this.state

		const logo = theme && theme.images && theme.images.logo ? theme.images.logo.url : undefined;
		const background = theme && theme.images && theme.images.background ? theme.images.background.url : undefined;

		// wrapping the App element with { theme && } prevents the app from
		// displaying unstyled content until the theme can be applied, sort've like ng-cloak
		return (
			<CustomProperties properties={formatThemeProperties(theme ? theme.properties : {})}>
				{theme && (
					<div className="App" style={{ backgroundImage: `url('${background}')` }}>
						<Header logo={logo} to={rooms.length === 1 ? `/room/${rooms[0].id}` : "/"} />
						<div className="main-content">
							<Route
								exact
								path="/"
								render={props => {
									let list = [...rooms]

									// If there is only one room, then we prevent the user from accessing the RoomsList component
									// this redirects the user back to the only room's overview
									if (list.length < 2 && typeof list[0] !== "undefined") {
										return <Redirect to={`/room/${list[0].id}`} />
									}

									// filter rooms based on the defaultRoom's settings
									if (typeof this.state.defaultRoom !== "undefined") {
										const defaultRoom = list.find(
											room => room.id.toString() === this.state.defaultRoom.toString()
										)

										// filter rooms if this room cannot see all rooms.
										if (defaultRoom && !defaultRoom.view_all) {
											// return any rooms that aren't hidden and are in the same group
											list = this.state.rooms.filter(
												room => room.group === defaultRoom.group && !room.hidden
											)
										}
									}

									return <RoomsList events={events} rooms={list} {...props} />
								}}
							/>

							<Route
								path="/room/:id"
								render={props => {
									const room = this.state.rooms.find(
										room => room.id.toString() === props.match.params.id.toString()
									)

									if (!room) {
										return <Redirect to="/" />
									}

									const events = this.state.events[room.id] ? this.state.events[room.id] : []
									const siblingCount = this.state.rooms.filter(rm => (room.view_all || rm.group.toString() === room.group.toString()) && rm.id !== room.id).length

									return (
										<Room
											pushEvent={this.pushEvent}
											removeEvent={this.removePendingEvents}
											updateEvents={this.updateEvents}
											onEventCreated={this.createEvent}
											events={Array.isArray(events) ? events : []}
											siblingCount={siblingCount}
											room={room}
											{...props}
										/>
									)
								}}
							/>
						</div>
					</div>
				)}
			</CustomProperties>
		)
	}
}

export default App
