import React, { Component } from 'react';
import moment from "moment/moment.js"

import "./Clock.css"

class Clock extends Component {
	constructor() {
	super()		
		this.state = {
			time: undefined,
			date: undefined
		}
	}

	componentDidMount() {
		this.setTime();
	}

	setTime() {
		const update = () => {
			const now = moment();
			
			this.setState({
				time: now.format('h:mm:ssa'),
				date: now.format('dddd, MMMM Do, YYYY')
			})			
		}

		update();
		
		setInterval(update, 1000);
	}

	render() {
	return (
		<div className="Clock">
			<h4>{ this.state.date }</h4>
			<span>{ this.state.time }</span>
		</div>
	)
	}
}


export default Clock