import React from 'react';

import { Link } from "react-router-dom"

import Clock from "./Clock/"

import defaultLogo from "../../../logo.png"

import './Header.css';

const Header = (props) => {
	
	const logo = props.logo ? props.logo : defaultLogo;

	return (
		<div className="Header">
			<Link to={props.to ? props.to : '/'}>
				<div className="branding">
					<img className="branding__logo" alt="Logo" src={logo} />
				</div>
			</Link>

			<div className="Header__right">
				<Clock />
			</div>
		</div>
		)
}

export default Header 