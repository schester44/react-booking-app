const formatThemeProperties = props => {
	const transformed = {}

	if (!props) {
		return transformed;
	}
	
	Object.keys(props).forEach(key => {
		transformed[key] = props[key].value
	})

	return transformed
}

export default formatThemeProperties