import moment from "moment"

const now = moment()
const currentNumericTime = () => now.hour() + parseFloat((now.minute() / 60).toFixed(2))

export default currentNumericTime