import moment from "moment";

const expandStartTimes = times => {
  let res = []

  for (let i = 0; i < times.length; i++) {
    let time = times[i]
    let ampm = time >= 12 ? "pm" : "am"

    let displayTime = time > 12 ? time - 12 : time

    res.push({ value: time, display: `${displayTime}:00${ampm}`, moment: moment({ hours: time, minutes: 0 }) })
    res.push({ value: time + 0.25, display: `${displayTime}:15${ampm}`, moment: moment({ hours: time, minutes: 15 }) })
    res.push({ value: time + 0.5, display: `${displayTime}:30${ampm}`, moment: moment({ hours: time, minutes: 30 }) })
    res.push({ value: time + 0.75, display: `${displayTime}:45${ampm}`, moment: moment({ hours: time, minutes: 45 }) })
  }

  return res
}

export default expandStartTimes
