import formatNumberToTime from "./format-number-to-time"

const formatStartTimes = (times, showAMPM) => {
  let transformed = []

    for (let i = 0; i < times.length; i++) {
		const time = times[i];
   		const display = formatNumberToTime(time, showAMPM);

		transformed.push({ display, value: time })
    }

    return transformed;
}

export default formatStartTimes