import moment from "moment"

const now = moment()

const getRoomStatus = (events) => {	
	const minutes = parseFloat(now.format('m'), 10);		
	const time = parseFloat(now.format('k'), 10) + (minutes / 60);

	return events.find(event => parseFloat(event.start) < time && parseFloat(event.end) > time && event.status !== 'pending')
}

export default getRoomStatus