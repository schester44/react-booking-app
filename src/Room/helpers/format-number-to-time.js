const formatNumberToTime = (time, showAMPM) => {

	const ampm = time >= 12 && time / 12 < 2 ? "pm" : "am"

	let hour = time <= 12 ? time : (time - 12 < 1 ? 12 : time - 12);
		hour  = hour > 12 ? hour - 12 : hour; // work around for when a time extends past 12am (24 hours)

	let minutes = time % 1 * 60

	if (isNaN(minutes)) {
		minutes = 0
	}

	const displayMinutes = minutes === 0 ? ":00" : `:${minutes}`

	return `${Math.floor(hour)}${displayMinutes}${showAMPM ? ampm : ''}`
}

export default formatNumberToTime