import React, { Component } from "react"

import { Route } from "react-router-dom"

import classNames from "classnames"

import Timeline from "../Timeline"
import Form from "./components/Form"
import Overview from "./components/Overview"

import { EventEmitter } from "fbemitter"

import { generateUid, expandStartTimes, formatStartTimes, getRoomStatus, currentNumericTime } from "./helpers"

class Room extends Component {
	constructor() {
		super()

		this.defaultTimes = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
		this.showTimeline = true

		this.state = {
			roomStatus: undefined,
			availableTimes: expandStartTimes(this.defaultTimes),
			maxMeetingLength: 4,
			form: {
				id: generateUid(),
				title: "",
				start: 7,
				duration: 0,
				status: "pending",
				end: 7.25
			}
		}

		this.defaultState = { ...this.state }


		this.onInputChange = this.onInputChange.bind(this)
		this.insertEvent = this.insertEvent.bind(this)
		this.onSubmitEvent = this.onSubmitEvent.bind(this)
		this.onFormCancel = this.onFormCancel.bind(this)
	}

	componentWillMount() {
		this.emitter = new EventEmitter()

		
		if (this.props.events) {
			this.checkRoomStatus()
			this.setAvailableTimes()
		}
	}

	componentWillReceiveProps() {
		if (this.props.events) {
			this.setAvailableTimes()
			this.checkRoomStatus()
		}
	}

	componentDidMount() {
		const windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

		// hide the tiemline if we're on a phone
		if (windowWidth < 680) {
			this.showTimeline = false
		}
	}

	setAvailableTimes() {
		const times = expandStartTimes(this.defaultTimes).map(time => time.value)

		const date = new Date()

		this.availableTimes = times.filter(time => {
			// rooms aren't occupied if the event is pending.
			// this checks that the time is not within event.start or event.end
			const occupied = this.props.events.some(event => time >= event.start && time < event.end && event.status !== "pending")
			
			if (occupied || time < date.getHours() - 0.5) {
				return false
			}
			
			return true
		})

		this.setState(oldState => {
			let start = this.availableTimes.indexOf(oldState.form.start) >= 0 ? oldState.form.start : this.availableTimes[0]

			let end = this.availableTimes.indexOf(oldState.form.start) >= 0
				? oldState.form.end
				: this.availableTimes[0] + 0.25

			// Sets the start time to the top of the hour,
			// only if the time is available and the user isn't trying to change the start time
			if (start !== oldState.form.start && oldState.form.duration === 0 && this.availableTimes.indexOf(date.getHours()) >= 0) {
				start = date.getHours()
			}

			const eventsAfterStart = this.props.events
				.filter(event => event.status !== "pending" && event.start > start)
				.sort((a, b) => a.start > b.start)


			const maxMeetingLength = eventsAfterStart[0] ? eventsAfterStart[0].start - start : this.defaultState.maxMeetingLength

			return {
				maxMeetingLength,
				availableTimes: formatStartTimes(this.availableTimes, true),
				form: { ...oldState.form, start, end }
			}
		})
	}

	/**
	 * Sets whether the room is available or not
	 */
	checkRoomStatus() {
		const unavailable = getRoomStatus(this.props.events)

		let newState = {
			roomStatus: unavailable ? "unavailable" : "available",
			currentMeeting: unavailable,
			isAvailable: typeof unavailable === "undefined" || false
		}

		// Sorted list of any events starting after the current time.
		const eventsAfterNow = this.props.events
			.sort((a, b) => a.start - b.start)
			.filter(event => event.start >= currentNumericTime())

		const recursivelySetNextAvailableTime = (event, events, key) => {
			const nextEvent = events[key + 1] 
			if (!nextEvent) {
				return event;
			}

			// If the current event's end time is the same time as the next events start time,
			// then there is no gap between them, so this events end time is not a good 'next available time' since an event happens right after it.
			if (event.end === nextEvent.start) {
				return recursivelySetNextAvailableTime(nextEvent, events, key + 1)
			}
		}

		newState.nextAvailableTime = unavailable ? unavailable.end : undefined

		if (eventsAfterNow && eventsAfterNow[0]) {	
			newState.nextMeeting = eventsAfterNow[0]	

			// if there is a meeting in session and there is a meeting within 15 minutes within the ending of the current meeting in session
			// then we recursively loop eventsAfterNow to get the next available time
			// else, we set nextAvailableTime to be the end time of the current meeting in session			
			if (unavailable) {
				if (eventsAfterNow[0].start - 0.15 <= unavailable.end) {
					const nextAvailableTime = recursivelySetNextAvailableTime(eventsAfterNow[0], eventsAfterNow, 0);

					if ( nextAvailableTime ) {
						newState.nextAvailableTime = nextAvailableTime.end
					}
				}
			} else {
				newState.nextAvailableTime = eventsAfterNow[0].start
			}
		}

		this.setState(newState)
	}

	/**
	 * Inserts/Updates pending events by calling prop functions to push and update the current pending event 
	 * @return {void}
	 */
	insertEvent() {
		const exists = this.props.events.some(event => event.id === this.state.form.id)

		// if it doesn't exist then use pushEvent to push the event into the current room's event list
		if (!exists) {
			this.props.pushEvent(this.props.room.id, this.state.form)
		} else {
			// get a list of events, update the event who shares the same ID as the current form
			const events = this.props.events.map(event => (event.id === this.state.form.id ? this.state.form : event))
			// set new state
			this.props.updateEvents(this.props.room.id, events)
		}
	}

	/**
	 * Called when the user submits teh form.
	 * Resets the form then calls a prop function to actually create the event.
	 * @return {[type]} [description]
	 */
	onSubmitEvent() {
		const callback = () => {
			// reset the form and assign a new unique id
			this.setState({
				form: Object.assign({}, this.defaultState.form, { id: generateUid() })
			})

			this.setAvailableTimes()
			this.checkRoomStatus()
		}

		this.props.onEventCreated(this.props.room.id, callback)
	}

	/**
	 * Called when the user clicks Cancel
	 * Removes the pending event from the room's list of events
	 * @return {void}
	 */
	onFormCancel() {
		this.props.removeEvent(this.props.room.id, this.state.form)
	}

	/**
	 * on "change" event handler for all form inputs in Room/Form
	 * @param  {object} event - event object from the change event. This value will be an integer for the range slider
	 * @param  {string} key - the name of the input 
	 * @param  {func} validationCallback - validation function to be called after state has been set
	 * @return {void}
	 */
	onInputChange(event, key, validationCallback) {
		// This is called after setState has ran
		const cb = () => {
			this.insertEvent()
			validationCallback()
		}

		// the duration range sends a value instead of an event
		let value = key === "duration" ? event : event.target.value

		// start field comes over as a string, parse it
		if (key === "start") {
			value = parseFloat(event.target.value)
		}

		// we need to update the end time any time were changing a time related field
		const end = parseFloat(key === "start" ? event.target.value : this.state.form.start, 10) +
						(key === "duration" ? event : this.state.form.duration)

		// focus the timeline if we're changing a time related field
		if (key === "duration" || key === "start") {
			this.emitter.emit("focus-timeline", end)
		}

		this.setState(oldState => {
			return {
				form: {
					...this.state.form,
					end,
					[key]: value
				}
			}
		}, cb)
	}

	render() {
		const { room, match, events, siblingCount } = this.props
		const { form, availableTimes, roomStatus, currentMeeting, nextMeeting, maxMeetingLength, nextAvailableTime } = this.state

		const mainClasses = {
			Room: true,
			"two-column": this.showTimeline,
			"one-column": !this.showTimeline
		}

		return (
			<div className={classNames(mainClasses)}>
				<div className="column">
					<Route
						exact
						path={`${match.url}`}
						render={props => (
							<Overview
								nextMeeting={nextMeeting}
								nextAvailableTime={nextAvailableTime}
								currentMeeting={currentMeeting}
								roomStatus={roomStatus}
								siblingCount={siblingCount}
								room={room}
								{...props}
							/>
						)}
					/>

					<Route
						exact
						path={`${match.url}/form`}
						render={props => (
							<Form
								form={form}
								maxMeetingLength={maxMeetingLength}
								availableTimes={availableTimes}
								room={room}
								onInputChange={this.onInputChange}
								onCancel={this.onFormCancel}
								createEvent={this.onSubmitEvent}
								{...props}
							/>
						)}
					/>
				</div>
				{ this.showTimeline && (
					<div className="column">
						<Timeline emitter={this.emitter} roomStatus={roomStatus} events={events} />
					</div>
				) }
			</div>
		)
	}
}

export default Room
