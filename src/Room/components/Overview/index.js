import React, { Component } from "react"
import "./Overview.css"

import { Link } from "react-router-dom"
import { formatNumberToTime } from "../../helpers"

import Status from "./components/Status"

class Overview extends Component {

  render() {

    const { match, room, roomStatus, currentMeeting, nextMeeting, siblingCount, nextAvailableTime } = this.props

    return (
      <div className="column-inner Overview">
        <i className="fa fa-calendar"></i>
        <h1 className="page-title">{room.name}</h1>
        <Status status={roomStatus} nextAvailableTime={nextAvailableTime} />

        { currentMeeting &&
          <div className="overview__current-meeting">
            <h3>Now: </h3>
            <h2>{ currentMeeting.title }</h2>
            <h2>{ formatNumberToTime( currentMeeting.start, true) } - { formatNumberToTime( currentMeeting.end, true ) }</h2>
          </div>
        }

        { nextMeeting && 
          <div className="overview__next-meeting">
            <h3>Next Meeting: </h3>
            <h2>{ nextMeeting.title }</h2>
            <h3>{ formatNumberToTime( nextMeeting.start, true) } - { formatNumberToTime( nextMeeting.end, true) }</h3>
          </div>
        }

        <div className="overview__action-btns push-to-bottom">
          <Link
            className="btn btn--brand btn--bold btn--large btn--rounded"
            role="button"
            to={`${match.url}/form`}>Book This Room</Link>
         
          {
            siblingCount > 0 && 
            <Link
              className="btn btn--semi-clear btn--medium btn--rounded"
              role="button"
              to="/">View Other Rooms</Link>
          }
        </div>

      </div>
    )
  }
}

export default Overview
