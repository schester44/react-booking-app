import React from "react"
import { formatNumberToTime } from "../../../../helpers"

const Status = props => {
	const { status, nextAvailableTime } = props
	
	return (
		<div>
			{nextAvailableTime && (
				<h2 className="overview__status">
					<span className={status}>{status}</span> until {formatNumberToTime(nextAvailableTime, true)}
				</h2>
			)}

			{!nextAvailableTime &&
			status && (
				<h2 className="overview__status">
					Status: <span className={status}>{status}</span>
				</h2>
			)}
		</div>
	)
}

export default Status
