import React from "react"

import { formatNumberToTime } from "../../../../helpers"

import CalendarIcon from "./CalendarIcon.js"

import "./ConfirmDetails.css"

const ConfirmDetails = (props) => {
    const { room, meeting } = props

    return (
      <div className="ConfirmDetails">
        <div className="ConfirmDetails__icon">
            <CalendarIcon />
        </div>
        <div className="ConfirmDetails__details">
            <h3>You are booking the</h3>
            <h2>{room.name}</h2>
            <h3>for your meeting titled:</h3>
            <h2>{meeting.title}</h2>
            <h3>from {formatNumberToTime(meeting.start, true)} - {formatNumberToTime(meeting.end, true)}</h3>
        </div>
      </div>
    )
}

export default ConfirmDetails
