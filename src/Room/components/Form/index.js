import React, { Component } from "react"
import { Link } from "react-router-dom"
import debounce from "lodash/debounce"

import InputRange from "react-input-range"
import classNames from "classnames"

import ConfirmDetails from "./components/ConfirmDetails"

import "./Form.css"

class Form extends Component {
    constructor() {
        super()

        this.state = {
            showConfirm: false,
            invalid: true,
            error: undefined,
            field: undefined
        }

        this.onBookClick = this.onBookClick.bind(this)
        this.onCancelClick = this.onCancelClick.bind(this)
        this.onInputFocus = this.onInputFocus.bind(this)
        this.closeKeyboard = this.closeKeyboard.bind(this)
    }

    onInputFocus(event) {
        document.body.addEventListener("touchstart", this.closeKeyboard)
    }

    closeKeyboard(event) {
        // prevent the event from propagating if the user clicks an input
        // this prevents the keyboard from closing, only to re-open from onInputFocus
        if (event.target.nodeName.toLowerCase() === "input") {
            event.preventDefault()
            event.stopPropagation()
        }

        // lose focus
        if (this.refs.START_TIME_INPUT) {
            this.refs.START_TIME_INPUT.blur()
        }

        document.body.removeEventListener("touchstart", this.closeKeyboard)
    }

    onCancelClick(event) {

        if (this.state.showConfirm) {
            event.preventDefault()
            this.setState({ showConfirm: false })
        }

        if (typeof this.props.onCancel === "function") {
            this.props.onCancel()
        }
    }

    onBookClick(event) {
        if (this.state.showConfirm) {
            // the user has confirmed they want to book the room, so execute the createEvent prop
            if (typeof this.props.createEvent === "function") {
                this.props.createEvent()
            }
        } else {
            // if we haven't shown the confirm page yet, show it first and prevent the Link transition from happening
            event.preventDefault()
            this.setState({ showConfirm: true })
        }
    }

    isValidForm() {
        if (this.props.form.title.length <= 0) {
            this.setState(old => ({ field: "title", error: "Meeting title is required", invalid: true }))
            return false
        }

        if (this.props.form.duration <= 0) {
            this.setState(old => ({ field: "duration", error: "Meeting duration is required.", invalid: true }))
            return false
        }

        if (!this.props.form.start || this.props.form.start <= 0) {
            this.setState(old => ({ field: "start", error: "start time field is required.", invalid: true }))
            return false
        }

        this.setState(old => ({ field: undefined, error: undefined, invalid: false }))
        return true
    }

    onInputChange(event, name) {
        this.props.onInputChange(event, name, this.isValidForm.bind(this))
    }

    render() {
        const { form, room, availableTimes, maxMeetingLength } = this.props
        const { showConfirm } = this.state

        const updateRange = debounce(range => {
            this.onInputChange(range, "duration")
        }, 1)

        const bookBtnClasses = classNames({
            btn: true,
            "btn--brand": true,
            "btn--bold": true,
            "btn--large": true,
            "btn--rounded": true,
            "primary-button": true,
            "btn--disabled": this.state.invalid
        })

        const meetingLabelClasses = classNames({
            "float-left": form.duration > 2
        })

        return (
            <div className="column-inner Form">
                {showConfirm && <ConfirmDetails room={room} meeting={form} />}

                {!showConfirm && (
                    <div className="form-container">
                        <h1 className="page-title">Book This Room</h1>
                        <div className="form-group">
                            <label htmlFor="title">Meeting Title</label>

                            <input
                                name="title"
                                ref="START_TIME_INPUT"
                                type="text"
                                value={form.title}
                                onFocus={this.onInputFocus}
                                onChange={e => this.onInputChange(e, "title")}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="start">Start Time</label>
                            <select name="start" value={form.start} onChange={e => this.onInputChange(e, "start")}>
                                {availableTimes.map((time, key) => {
                                    return (
                                        <option key={key} value={time.value}>
                                            {time.display}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="duration" id="duration-label" className={meetingLabelClasses}>
                                Meeting Length
                            </label>
                            <InputRange
                                maxValue={maxMeetingLength}
                                minValue={0}
                                step={0.25}
                                value={form.duration}
                                formatLabel={value =>
                                    `${value < 1 ? value * 60 : value}${value < 1
                                        ? "mins"
                                        : value === 1 ? "hr" : "hrs"}`}
                                onChange={range => updateRange(range)}
                            />
                        </div>
                    </div>
                )}

                <div className="overview__action-btns">
                    <Link
                        className={bookBtnClasses}
                        onClick={e => this.onBookClick(e)}
                        role="button"
                        to={`/room/${room.id}`}
                    >
                        {showConfirm ? "Confirm" : "Book"}
                    </Link>

                    <Link
                        className="btn secondary-btn btn--semi-clear btn--medium btn--rounded"
                        onClick={e => this.onCancelClick(e)}
                        role="button"
                        to={`/room/${room.id}`}
                    >
                        {showConfirm ? "Change" : "Cancel"}
                    </Link>
                </div>
            </div>
        )
    }
}

export default Form
