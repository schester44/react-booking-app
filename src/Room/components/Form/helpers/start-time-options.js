const startTimeOptions = (times) => {
  let res = []

    for (let i = 0; i < times.length; i++) {
      let time = times[i];
      let ampm = time >= 12 ? 'pm' : 'am';
      
      let displayTime = time > 12 ? time - 12 : time;

      res.push({ value: time, display: `${displayTime}:00${ampm}` });
      res.push({ value: time + .25, display: `${displayTime}:15${ampm}` });
      res.push({ value: time + .50, display: `${displayTime}:30${ampm}` });
      res.push({ value: time + .75, display: `${displayTime}:45${ampm}` });
    }

    return res;
}

export default startTimeOptions;